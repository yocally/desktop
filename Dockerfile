FROM fedora:latest

ENV TZ="America/Chicago"

RUN dnf install -y \
	zsh \
	firefox \
	blackbox \
	supervisor \
	tigervnc-server \
	novnc \
	tigervnc \
	VirtualGL \
	procps-ng \
	xorg-x11-xauth

RUN vglserver_config -config +s +f +t

RUN useradd --shell /bin/zsh -U csibben \
	&& echo "csibben ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers

USER csibben:csibben

RUN touch /home/csibben/.Xauthority

CMD vncserver :90 \
	-SecurityTypes None \
	-alwaysshared \
	-fg
